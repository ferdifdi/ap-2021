package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class FacadeTransformationTest {
    private Class<?> facadeClass;

    @BeforeEach
    public void setup() throws Exception {
        facadeClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.FacadeTransformation");
    }

    @Test
    public void testFacadeHasEncodeMethod() throws Exception {
        Method translate = facadeClass.getDeclaredMethod("encode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testFacadeHasDecodeMethod() throws Exception {
        Method translate = facadeClass.getDeclaredMethod("decode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

}
