package id.ac.ui.cs.advprog.tutorial1.observer.core;

import java.util.List;

public class AgileAdventurer extends Adventurer {


    public AgileAdventurer(Guild guild) {
        this.name = "Agile";
        //ToDo: Complete Me
        this.guild = guild;
        this.guild.add(this);
    }

    //ToDo: Complete Me
    @Override
    public void update() {
        //ToDo:
        if(guild.getQuestType().equals("D") || guild.getQuestType().equals("R")) {
            getQuests().add(guild.getQuest());
        }
    }
}
