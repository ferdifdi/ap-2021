package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    protected ArrayList<Spell> ALSpell;

    public ChainSpell(ArrayList<Spell> ALSpell) {
        this.ALSpell = ALSpell;
    }

    @Override
    public void cast() {
        for(Spell spell : ALSpell) {
            spell.cast();
        }
    }

    @Override
    public void undo() {
        for(int i = ALSpell.size() - 1; i >= 0; i--) {
            ALSpell.get(i).undo();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
