package id.ac.ui.cs.advprog.tutorial2.command.repository;

import id.ac.ui.cs.advprog.tutorial2.command.core.spell.Spell;
import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.HighSpiritState;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class ContractSeal {

    private Map<String, Spell> spells;

    public ContractSeal() {
        spells = new HashMap<>();
    }

    private Spell latestSpell;

    public void registerSpell(Spell spell) {
        spells.put(spell.spellName(), spell);
    }

    public void castSpell(String spellName) {
        // TODO: Complete Me
        Spell spell = spells.get(spellName);
        this.latestSpell = spell;
        spell.cast();
    }

    public void undoSpell() {
        // TODO: Complete Me
        if(latestSpell == null) {

        }
        else {
            latestSpell.undo();
            latestSpell = null; //agar tidak 2x berturut-turut
        }
    }

    public Collection<Spell> getSpells() { return spells.values(); }
}
