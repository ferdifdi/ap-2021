package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class InuzumaRamenFactoryTest {
    private InuzumaRamenFactory inuzumaRamenFactory;

    @BeforeEach
    public void setUp() {
        inuzumaRamenFactory = new InuzumaRamenFactory();
    }

    @Test
    public void testProcessNoodleInuzumaRamenNotNull() throws Exception {
        assertNotNull(inuzumaRamenFactory.processNoodle());
    }

    @Test
    public void testProcessMeatInuzumaRamenNotNull() throws Exception {
        assertNotNull(inuzumaRamenFactory.processMeat());
    }

    @Test
    public void testProcessToppingInuzumaRamenNotNull() throws Exception {
        assertNotNull(inuzumaRamenFactory.processTopping());
    }

    @Test
    public void testProcessFlavorInuzumaRamenNotNull() throws Exception {
        assertNotNull(inuzumaRamenFactory.processFlavor());
    }

}
