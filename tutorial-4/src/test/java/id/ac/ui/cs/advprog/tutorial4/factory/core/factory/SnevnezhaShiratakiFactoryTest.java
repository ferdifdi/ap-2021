package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class SnevnezhaShiratakiFactoryTest {
    private SnevnezhaShiratakiFactory snevnezhaShiratakiFactory;

    @BeforeEach
    public void setUp() {
        snevnezhaShiratakiFactory = new SnevnezhaShiratakiFactory();
    }

    @Test
    public void testProcessNoodleSnevnezhaShiratakiNotNull() throws Exception {
        assertNotNull(snevnezhaShiratakiFactory.processNoodle());
    }

    @Test
    public void testProcessMeatSnevnezhaShiratakiNotNull() throws Exception {
        assertNotNull(snevnezhaShiratakiFactory.processMeat());
    }

    @Test
    public void testProcessToppingSnevnezhaShiratakiNotNull() throws Exception {
        assertNotNull(snevnezhaShiratakiFactory.processTopping());
    }

    @Test
    public void testProcessFlavorSnevnezhaShiratakiNotNull() throws Exception {
        assertNotNull(snevnezhaShiratakiFactory.processFlavor());
    }

}
