package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class FlavorTest {
    @Test
    public void testFlavorSaltyGetDescription() throws Exception {
        Salty salty = new Salty();
        assertEquals("Adding a pinch of salt...", salty.getDescription());
    }

    @Test
    public void testFlavorSpicyGetDescription() throws Exception {
        Spicy spicy = new Spicy();
        assertEquals("Adding Liyuan Chili Powder...", spicy.getDescription());
    }

    @Test
    public void testFlavorSweetGetDescription() throws Exception {
        Sweet sweet = new Sweet();
        assertEquals("Adding a dash of Sweet Soy Sauce...", sweet.getDescription());
    }

    @Test
    public void testFlavorUmamiGetDescription() throws Exception {
        Umami umami = new Umami();
        assertEquals("Adding WanPlus Specialty MSG flavoring...", umami.getDescription());
    }
}
