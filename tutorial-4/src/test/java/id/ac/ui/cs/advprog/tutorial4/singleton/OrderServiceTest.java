package id.ac.ui.cs.advprog.tutorial4.singleton;

import id.ac.ui.cs.advprog.tutorial4.singleton.service.OrderServiceImpl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class OrderServiceTest {
    OrderServiceImpl orderService;

    @BeforeEach
    public void setUp() {
        orderService = new OrderServiceImpl();
    }

    @Test
    public void testSingletonOrderServiceImplOrderADrink() throws Exception {
        orderService.orderADrink("Wine");
        assertEquals("Wine", orderService.getDrink().getDrink());
    }

    @Test
    public void testSingletonOrderServiceImplOrderADrinkError() throws Exception {
        orderService.orderADrink("Wine");
        assertNotEquals("Teh", orderService.getDrink().getDrink());
    }

    @Test
    public void testSingletonOrderServiceImplOrderAFood() throws Exception {
        orderService.orderAFood("Adeptus Temptation");
        assertEquals("Adeptus Temptation", orderService.getFood().getFood());
    }

    @Test
    public void testSingletonOrderServiceImplOrderAFoodError() throws Exception {
        orderService.orderAFood("Adeptus Temptation");
        assertNotEquals("Sweet Madame", orderService.getFood().getFood());
    }

    @Test
    public void testSingletonOrderServiceImplGetDrink() throws Exception {
        assertNotNull(orderService.getDrink());
    }

    @Test
    public void testSingletonOrderServiceImplGetFood() throws Exception {
        assertNotNull(orderService.getFood());
    }
}
