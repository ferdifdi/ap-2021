package id.ac.ui.cs.advprog.tutorial4.factory.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertNotNull;


public class MenuServiceTest {
    private MenuServiceImpl menuService;

    @BeforeEach
    public void setUp() {
        menuService = new MenuServiceImpl();
    }

    @Test
    public void testMenuServiceImplCreateMenu() throws Exception {
        menuService.createMenu("WanPlus Beef Mushroom Soba", "LiyuanSoba");
        menuService.createMenu("Bakufu Spicy Pork Ramen", "InuzumaRamen");
        menuService.createMenu("Good Hunter Cheese Chicken Udon", "MondoUdon");
        menuService.createMenu("Morepeko Flower Fish Shirataki", "SnevnezhaShirataki");
    }

    @Test
    public void testMenuServiceImplGetMenus() throws Exception {
        assertNotNull(menuService.getMenus());
    }

}
