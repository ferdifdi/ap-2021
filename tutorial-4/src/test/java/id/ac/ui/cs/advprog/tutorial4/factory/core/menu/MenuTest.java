package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class MenuTest {
    private Menu menu;

    @BeforeEach
    public void setUp() {
        menu = new InuzumaRamen("Wibu Ramen");
    }

    @Test
    public void testMenuGetName() throws Exception {
        assertEquals("Wibu Ramen", menu.getName());
    }

    @Test
    public void testMenuGetNoodleNotNull() throws Exception {
        assertNotNull(menu.getNoodle());
    }

    @Test
    public void testMenuGetMeatNotNull() throws Exception {
        assertNotNull(menu.getMeat());
    }

    @Test
    public void testMenuGetToppingNotNull() throws Exception {
        assertNotNull(menu.getTopping());
    }

    @Test
    public void testMenuGetFlavorNotNull() throws Exception {
        assertNotNull(menu.getFlavor());
    }

    @Test
    public void testMenuCreateMenuInuzumaRamen() throws Exception {
        InuzumaRamen inuzumaRamen = new InuzumaRamen("Wibu Ramen");
        assertNotNull(inuzumaRamen);
    }

    @Test
    public void testMenuCreateMenuLiyuanSoba() throws Exception {
        LiyuanSoba liyuanSoba = new LiyuanSoba("Spicy Soba by Xiangling");
        assertNotNull(liyuanSoba);
    }

    @Test
    public void testMenuCreateMenuMondoUdon() throws Exception {
        MondoUdon mondoUdon = new MondoUdon("Delicious Udon by Noelle");
        assertNotNull(mondoUdon);
    }

    @Test
    public void testMenuCreateMenuSnevnezhaShirataki() throws Exception {
        SnevnezhaShirataki snevnezhaShirataki = new SnevnezhaShirataki("Yummy Shirataki (Tsaritsa Choice)");
        assertNotNull(snevnezhaShirataki);
    }

}
