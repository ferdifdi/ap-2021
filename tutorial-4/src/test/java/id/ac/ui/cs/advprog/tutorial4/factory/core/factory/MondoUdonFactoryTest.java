package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class MondoUdonFactoryTest {
    private MondoUdonFactory mondoUdonFactory;

    @BeforeEach
    public void setUp() {
        mondoUdonFactory = new MondoUdonFactory();
    }

    @Test
    public void testProcessNoodleMondoUdonNotNull() throws Exception {
        assertNotNull(mondoUdonFactory.processNoodle());
    }

    @Test
    public void testProcessMeatMondoUdonNotNull() throws Exception {
        assertNotNull(mondoUdonFactory.processMeat());
    }

    @Test
    public void testProcessToppingMondoUdonNotNull() throws Exception {
        assertNotNull(mondoUdonFactory.processTopping());
    }

    @Test
    public void testProcessFlavorMondoUdonNotNull() throws Exception {
        assertNotNull(mondoUdonFactory.processFlavor());
    }

}
