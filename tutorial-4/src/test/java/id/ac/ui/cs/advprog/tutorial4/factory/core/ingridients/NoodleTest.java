package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class NoodleTest {
    @Test
    public void testNoodleRamenGetDescription() throws Exception {
        Ramen ramen = new Ramen();
        assertEquals("Adding Inuzuma Ramen Noodles...", ramen.getDescription());
    }

    @Test
    public void testNoodleShiratakiGetDescription() throws Exception {
        Shirataki shirataki = new Shirataki();
        assertEquals("Adding Snevnezha Shirataki Noodles...", shirataki.getDescription());
    }

    @Test
    public void testNoodleSobaGetDescription() throws Exception {
        Soba soba = new Soba();
        assertEquals("Adding Liyuan Soba Noodles...", soba.getDescription());
    }

    @Test
    public void testNoodleUdonGetDescription() throws Exception {
        Udon udon = new Udon();
        assertEquals("Adding Mondo Udon Noodles...", udon.getDescription());
    }
}
