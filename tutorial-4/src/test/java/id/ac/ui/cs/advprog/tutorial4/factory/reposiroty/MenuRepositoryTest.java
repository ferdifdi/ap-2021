package id.ac.ui.cs.advprog.tutorial4.factory.reposiroty;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba;
import id.ac.ui.cs.advprog.tutorial4.factory.repository.MenuRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class MenuRepositoryTest {

    private MenuRepository menuRepository;

    @BeforeEach
    public void setUp() {
        menuRepository = new MenuRepository();
    }

    @Test
    public void testMenuRepositoryGetMenus() throws Exception {
        assertNotNull(menuRepository.getMenus());
    }

    @Test
    public void testMenuRepositoryAdd() throws Exception {
        menuRepository.add(new LiyuanSoba("Liyuan Soba Spicy by Xiangling"));
        assertEquals(1, (menuRepository.getMenus().size()));
    }

}
