package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class LiyuanSobaFactoryTest {
    private LiyuanSobaFactory liyuanSobaFactory;

    @BeforeEach
    public void setUp() {
        liyuanSobaFactory = new LiyuanSobaFactory();
    }

    @Test
    public void testProcessNoodleLiyuanSobaNotNull() throws Exception {
        assertNotNull(liyuanSobaFactory.processNoodle());
    }

    @Test
    public void testProcessMeatLiyuanSobaNotNull() throws Exception {
        assertNotNull(liyuanSobaFactory.processMeat());
    }

    @Test
    public void testProcessToppingLiyuanSobaNotNull() throws Exception {
        assertNotNull(liyuanSobaFactory.processTopping());
    }

    @Test
    public void testProcessFlavorLiyuanSobaNotNull() throws Exception {
        assertNotNull(liyuanSobaFactory.processFlavor());
    }

}
