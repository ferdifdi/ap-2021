package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.*;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.*;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.*;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.*;

public class SnevnezhaShiratakiFactory implements IngredientsFactory {

    //Ingridients:
    //Noodle: Shirataki
    //Meat: Fish
    //Topping: Flower
    //Flavor: Umami

    @Override
    public Noodle processNoodle() {
        return new Shirataki();
    }
    @Override
    public Meat processMeat() {
        return new Fish();
    }
    @Override
    public Topping processTopping(){
        return new Flower();
    }
    @Override
    public Flavor processFlavor(){
        return new Umami();
    }

}
