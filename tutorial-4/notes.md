Perbedaan 

- Lazy instantiation approach 
  Malas membuat instance. Hanya buat ketika diperlukan saja, bukan pada saat awal program dieksekusi.
  
  Pada OrderDrink.java,
  
  //field nya null
  private static OrderDrink orderDrink = null;
  
  //Baru ketika diperlukan, dibuat
  //Todo : Complete Me with lazy instantiation approach
  public static OrderDrink getInstance() {
  if (orderDrink == null) {
  orderDrink = new OrderDrink();
  }
  return orderDrink;
  }
  
  Keuntungannya, bagus buat field yg belum tentu dibutuhkan.
  Sehingga meningkatkan performance.
  Misalnya order minum karena biasanya udah bawa minum dari rumah.
  Kekurangannya, kalau field yang pasti dibutuhkan,
  harus buat instance dulu.
  
- Eager Instantiation Approach
  Serakah membuat instance. Langsung membuat instance pada awal eksekusi program.
  
  Pada orderFood.java
    
  //Langsung dibuat instance
  private static OrderFood orderFood = new OrderFood();
    
  //Kalau mau buat instance, langsung return karena sudah ada
  //Todo : Complete Me with eager instantiation approach
  public static OrderFood getInstance() {
  return orderFood;
  }
  
  Kelebihannya cocok buat sesuatu yang udah pasti dibutuhkan,
  seperti makanan. Jadi tidak perlu buat instance baru lagi,
  langsung direturn.
  Kekurangannya, tidak cocok buat sesuatu yang belum pasti dibutuhkan,
  karena nanti dapat mengurangi performance program.